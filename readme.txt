SIMPLE FILE SERVER
by Juanmi López


DESCRIPTION

Server program that uses HTTP methods to manage files and front-end
interface that lets operate the server through a network.

The server runs on node.js. The user interface is implemented using
HTML5 and JavaScript and is meant to be used through a web browser.
JavaScript ES6+ and the fetch API are used, browsers released from 2016
onwards should be able to run it without problems.


USAGE

Use node.js to install the required node_modules and start the server.
Install the modules with:

	npm install

Start the server:

	node server.js

The server is now listening to HTTP requests in port 8000.

The user interface is defined in the file editor.html. Point your
browser to the editor.html file through port 8000 of your server. For
localhost that would be:

	http://localhost:8000/editor.html

The root folder managed by the program is called 'site'. Inside that
folder files can be uploaded, downloaded, created, deleted, opened,
read and edited through the front-end interface. Folders can also be
created, navigated and deleted.

The 'Open' action will navigate into the selected folder or open the
selected file in another browser window. Files with a 'text' mime type
can be viewed and edited through the front-end interface. In the 'File
View' panel, the 'Edit' button will open the 'File Edit' panel. In the
'File Edit' panel, the text contents of the opened file can be
modified. The 'Update' button will update the contens of the file in
the server ("save" the file), and the 'Retrieve' button will load the
text contents currently in the server into the editor. JavaScript and
JSON file types are also configured to be editable through the
front-end interface. The 'New file' button will create a file in the
current folder with the specified filename and jump to the 'File Edit'
panel where the contents of the new file can be defined. Note that the
filename can be anything, but if it is not followed by an extension
that makes the file recognizable as text it will not be possible to
edit the file through the front-end interface again (even if it was
possible just after creation). The 'Upload files' button lets upload
multiple files at once. The 'Delete' button lets erase multiple files
and folders at once.


DISCLAIMER

This software is made and tested using node.js version 12.14.1 and
Firefox 72 under GNU/Linux x86_64.
Licenses for the fonts used are in their respective folders. Licenses
used in code are in the relevant source files.
The favicon.ico file is from the Farm-fresh set by FatCow Web Hosting,
licensed under the terms found in
https://creativecommons.org/licenses/by/3.0/us/
This software is made mainly as an exercise. The author does not take
any responsibility for the use (or misuse) of it.
