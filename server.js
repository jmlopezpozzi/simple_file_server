/* Simple file server */

"use strict";


const {createServer} = require("http");

const methods = Object.create(null);

createServer((request, response) => {
	console.log(`req: ${request.url} ${request.method}`);
	let handler = methods[request.method] || notAllowed;
	handler(request)
		.catch(error => {
			if (error.status != null) return error;
			let ret = {body: String(error), status: 500};
			console.log(ret);
			return ret;
		})
		.then(({body, status = 200, type = "text/plain"}) => {
			if (type == "custom/directory") {
				response.writeHead(status, {
					"Content-Type": "application/json",
					"Is-Directory": "true"
				});
			}
			else {
				response.writeHead(status, {"Content-Type": type});
			}
			console.log(`status: ${status}, type: ${type}`);
			if (body && body.pipe) {
				body.pipe(response);
			}
			else response.end(body);
		});
	}).listen(8000);

async function notAllowed(request) {
	return {
		status: 405,
		body: `Method ${request.method} not allowed.`
	};
}


const {parse} = require("url");
const {resolve, sep} = require("path");

//const siteDirectory = "site";
const baseDirectory = process.cwd();

function urlPath(url) {
	let {pathname} = parse(url);
	let path = resolve(decodeURIComponent(pathname).slice(1));
	if (path != baseDirectory &&
	    !path.startsWith(baseDirectory + sep))
	{
		throw {status: 403, body: "Forbidden"};
	}
	return path;
}


const {createReadStream} = require("fs");
const {stat, readdir} = require("fs").promises;
const mime = require("mime");

methods.GET = async function(request) {
	let path = urlPath(request.url);
	let stats;
	try {
		stats = await stat(path);
	} catch (error) {
		if (error.code != "ENOENT") throw error;
		else return {status: 404, body: "File not found"};
	}
	if (stats.isDirectory()) {
		return {
			body: await generateFileListJSON(path),
			type: "custom/directory",  // This type is not standard!
		};
	}
	else {
		return {body: createReadStream(path), type: mime.getType(path)};
	}
};

async function generateFileListJSON(path) {
	let ls = await readdir(path);
	let fileList = [];
	for (let file of ls) {
		let filePath = path + "/" + file;
		let stats = await stat(filePath);
		let type = stats.isDirectory() ? "custom/directory" :
		                                 mime.getType(filePath);
		if (type == null) type = "text/plain";
		fileList.push({"name": file, "type": type});
	}
	let json = JSON.stringify(fileList);
	return json;
}


const {rmdir, unlink} = require("fs").promises;

methods.DELETE = async function(request) {
	let path = urlPath(request.url);
	let stats;
	try {
		stats = await stat(path);
	} catch (error) {
		if (error.code != "ENOENT") throw error;
		else return {status: 204};
	}
	if (stats.isDirectory()) await rmdir(path);
	else await unlink(path);
	return {status: 204};
};


const {createWriteStream} = require("fs");

function pipeStream(from, to) {
	return new Promise((resolve, reject) => {
		from.on("error", reject);
		to.on("error", reject);
		to.on("finish", resolve);
		from.pipe(to);
	});
}

methods.PUT = async function(request) {
	let path = urlPath(request.url);
	await pipeStream(request, createWriteStream(path));
	return {status: 204};
};


const {mkdir} = require("fs").promises;

methods.MKCOL = async function(request) {
  let path = urlPath(request.url);
  let stats;
  try {
    stats = await stat(path);
  } catch (error) {
    if (error.code != "ENOENT") throw error;
    await mkdir(path);
    return {status: 204};
  }
  if (stats.isDirectory()) return {status: 204};
  else return {status: 400, body: "Not a directory"};
};



/*
This file uses code published under the following license:


Copyright 2018  Marijn Haverbeke

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/
