/* Simple file server frontend */

"use strict";


const baseDirectory = "site";

const fileListElement = document.getElementById("file_list");
const contentViewElement = document.getElementById("file_view_content");
contentViewElement.value = "";

let currentPath = baseDirectory;  // Path for the current directory in the File List activity

let fileTextPath = "";     // Path for the open file in the File View and File Edit activities
let fileTextContent = "";  // Contents of the open file in the File View and File Edit activities

let lastTextLoadId;  // When a text file is loaded (for View or Edit) an ID is generated. This variable holds the last one.


const pathNameElement = document.getElementById("path_name");


const forbiddenPaths = new Map();  // Holds paths of files and directories being uploaded or pending of deletion

function pathIsForbidden(path) {
	for (let arr of forbiddenPaths.values()) {
		if (arr.includes(path)) {
			alert("File " + path + " is being uploaded or pending of deletion");
			return true;
		}
	}
	return false;
}


function genericAlert(error) {
	alert("Error when talking to server: " + error);
}


function showActivity(newActivity) {
	const activities = {
		list: document.forms["file_list_activity"],
		view: document.forms["file_view_activity"],
		edit: document.forms["file_edit_activity"],
	};

	for (let activity of Object.keys(activities)) {
		if (newActivity == activity) activities[activity].style.display = "";
		else activities[activity].style.display = "none";
	}

	let activityNameElement = document.getElementById("activity_name");
	let name;
	switch (newActivity) {
	case "list":
		name = "FILE LIST";
		pathNameElement.textContent = currentPath;
		break;
	case "view":
		name = "FILE VIEW";
		break;
	case "edit":
		name = "FILE EDIT";
		break;
	default:
		console.log("Unknown activity " + newActivity);
		name = "";
		break;
	}
	activityNameElement.textContent = name;
}


/* File list */

function updateFileList(path) {
	return fetch(path)
		.then((response) => {
			return response.json();
		})
		.then((ls) => {
			while (fileListElement.childNodes.length > 0) {
				fileListElement.firstChild.remove();
			}
			currentPath = path;
			pathNameElement.textContent = path;
			if (ls.length == 0) {
				return;
			}
			populateFileListSelect(ls, fileListElement);
		})
		.catch(error => alert)
		.finally(() => fileListElement.dispatchEvent(new Event("change")));
}

function populateFileListSelect(list, parent) {
	for (let file of list) {
		let option = document.createElement("option");
		option.value = file.name;
		let operationType = getOperationType(file.type);
		option.setAttribute("data-opttype", operationType);
		option.appendChild(document.createTextNode(file.name));
		parent.appendChild(option);
	}
}


function getOperationType(type) {
	if (type == "custom/directory") return "dir";
	if (isTextCompatible(type)) return "txt";
	return "bin";
}

function isTextCompatible(type) {
	let result = false;
	if (type.startsWith("text/") ||
	    type == "application/javascript" ||
	    type == "application/json")
	{
		result = true;
	}
	return result;
}


function getFileAsText(path) {
	return fetch(path)
		.then((response) => {
			return response.text();
		})
		.then((text) => {
			return {text, path};
		})
		.catch(error => genericAlert);
}


const listOpenButton = document.getElementById("list_open_button");
listOpenButton.addEventListener("click", listOpenButtonAction);

function listOpenButtonAction(event) {
	event.preventDefault();
	let list = document.forms.file_list_activity.elements.file_list;
	if (list.selectedIndex == -1 || list.selectedOptions.length > 1) {
		return;
	}
	let selected = list.options[list.selectedIndex];
	let targetPath = currentPath + "/" + selected.value;
	if (pathIsForbidden(targetPath)) return;
	let operationType = selected.dataset.opttype;
	if (operationType == "dir") {
		updateFileList(targetPath);
	}
	else {
		window.open(targetPath, "_blank");
	}
}


const listViewButton = document.getElementById("list_view_button");
listViewButton.addEventListener("click", listViewButtonAction);

function listViewButtonAction(event) {
	event.preventDefault();
	textOperationAction("view");
}


const listEditButton = document.getElementById("list_edit_button");
listEditButton.addEventListener("click", listEditButtonAction);

function listEditButtonAction(event) {
	event.preventDefault();
	textOperationAction("edit");
}


function textOperationAction(operation) {
	const contentElements = {
		"view": contentViewElement,
		"edit": contentEditElement,
	};

	contentViewElement.value = "";
	contentEditElement.value = "";
	fileTextContent = "";
	fileTextPath = "";

	let list = document.forms.file_list_activity.elements.file_list;
	if (list.selectedIndex == -1 || list.selectedOptions.length > 1) {
		return;
	}
	let selected = list.options[list.selectedIndex];
	let targetPath = currentPath + "/" + selected.value;
	if (pathIsForbidden(targetPath)) return;
	let operationType = selected.dataset.opttype;
	if (operationType != "txt") return;
	let textLoadId = Math.random();
	lastTextLoadId = textLoadId;

	return getFileAsText(targetPath)
		.then((textData) => {
			if (lastTextLoadId == textLoadId) {
				fileTextPath = textData.path;
				fileTextContent = textData.text;
				showActivity(operation);
				contentElements[operation].value = textData.text;
				pathNameElement.textContent = textData.path;
			}
		});
}


const listBackButton = document.getElementById("list_back_button");
listBackButton.addEventListener("click", listBackButtonAction);

function listBackButtonAction(event) {
	event.preventDefault();
	if (currentPath == baseDirectory) return;
	let path = currentPath.slice(0, currentPath.lastIndexOf("/"));
	return updateFileList(path);
}


function enterName(itemType) {
	let name = prompt("Enter " + itemType + " name:");
	if (name == null || name.startsWith(".") || name.includes("/")) {
		return false;
	}
	return name;
}


const mkdirButton = document.getElementById("new_directory_button");
mkdirButton.addEventListener("click", mkdirButtonAction);

function mkdirButtonAction(event) {
	event.preventDefault();
	let name = enterName("directory");
	if (!name) return;
	let newDirPath = currentPath;
	fetch(newDirPath + "/" + name, {method: "MKCOL"})
		.then(() => {
			if (currentPath == newDirPath) {
				return updateFileList(currentPath);  // Only called if the user didn't change folder
			}
		}, genericAlert);
}


const newFileButton = document.getElementById("new_file_button");
newFileButton.addEventListener("click", newFileButtonAction);

function newFileButtonAction(event) {
	event.preventDefault();
	let name = enterName("file");
	if (!name) return;
	let existingFiles = fileListElement.options;
	for (let file of existingFiles) {
		if (name == file.value) {
			let overwrite = confirm("A file with name " + name +
			                        " already exists. Overwrite?");
			if (!overwrite) return;
		}
	}
	let path = currentPath + "/" + name;
	fetch(path, {method: "PUT"})
		.then(() => {
			updateFileList(currentPath);
			fileTextPath = path;
			fileTextContent = "";
			contentViewElement.value = "";
			contentEditElement.value = "";
			showActivity("edit");
			pathNameElement.textContent = path;
		})
		.catch((err) => {
			alert("Could not create file " + name + ": " + err);
		});
}


const uploadControl = document.getElementById("upload_file_input");
uploadControl.addEventListener("change", uploadControlAction);
const uploadFileButton = document.getElementById("upload_file_button");
uploadFileButton.addEventListener("click", (event) => {
	event.preventDefault();
	uploadControl.click();
});

function uploadControlAction() {
	let uploadPath = currentPath;
	let mapKey = Math.random();
	let filesBeingUploaded = [];
	forbiddenPaths.set(mapKey, filesBeingUploaded);
	let uploadCommands = [];
	for (let i = 0; i < uploadControl.files.length; i += 1) {
		let filePath = uploadPath + "/" + uploadControl.files[i].name;
		filesBeingUploaded.push(filePath);
		let command = fetch(filePath,
		                    {method: "PUT", body: uploadControl.files[i]})  // fetch accepts Blob type as its body. File type, returned by the input type="file" implements Blob.
			.then(() => {
				filesBeingUploaded[i] = "";  // File is already uploaded, access is not forbidden anymore
				if (currentPath == uploadPath) {
					return updateFileList(uploadPath);  // Called for every uploaded file as long as the user remains in the upload folder
				}
			})
			.catch((err) => {
				alert("Could not upload file " + uploadControl.files[i].name +
				      ": " + err);
			});
		uploadCommands.push(command);
	}
	Promise.all(uploadCommands)
		.finally(() => forbiddenPaths.delete(mapKey));
}


const deleteButton = document.getElementById("delete_button");
deleteButton.addEventListener("click", deleteButtonAction);

function deleteButtonAction(event) {
	event.preventDefault();
	let basePath = currentPath;
	let list = document.forms.file_list_activity.elements.file_list;
	if (list == undefined) return;
	if (list.selectedIndex == -1) return;
	let mapKey = Math.random();
	let filesBeingDeleted = [];
	forbiddenPaths.set(mapKey, filesBeingDeleted);
	let selected = list.selectedOptions;
	let deleteCommands = [];
	for (let option of selected) {
		let file = currentPath + "/" + option.value;
		if (pathIsForbidden(file)) continue;
		filesBeingDeleted.push(file);
		let command = fetch(file, {method: "DELETE"})
			.catch((error) => {
				alert("Delete operation for file " + file + " failed: " +
				      error);
			});
		deleteCommands.push(command);
	}
	Promise.all(deleteCommands)
		.finally(() => {
			forbiddenPaths.delete(mapKey);
			if (currentPath == basePath) {
				return updateFileList(basePath);  // Only called if the user didn't change folder
			}
		});
}


const refreshButton = document.getElementById("refresh_button");
refreshButton.addEventListener("click", refreshButtonAction);

function refreshButtonAction(event) {
	event.preventDefault();
	return updateFileList(currentPath);
}


fileListElement.addEventListener("change", fileListElementInputAction);

function fileListElementInputAction() {
	let selectedIndex = fileListElement.selectedIndex;
	if (selectedIndex == -1) {
		listOpenButton.disabled = true;
		listViewButton.disabled = true;
		listEditButton.disabled = true;
		deleteButton.disabled = true;
		return;
	}
	if (fileListElement.selectedOptions.length > 1) {
		listOpenButton.disabled = true;
		listViewButton.disabled = true;
		listEditButton.disabled = true;
		deleteButton.disabled = false;
		return;
	}
	let opttype = fileListElement.options[selectedIndex].dataset.opttype;
	if (opttype != "txt") {
		listOpenButton.disabled = false;
		listViewButton.disabled = true;
		listEditButton.disabled = true;
		deleteButton.disabled = false;
	}
	else {
		listOpenButton.disabled = false;
		listViewButton.disabled = false;
		listEditButton.disabled = false;
		deleteButton.disabled = false;
	}
}


/* File view */

contentViewElement.addEventListener("input", () => {
	contentViewElement.value = fileTextContent;
	contentViewElement.blur();
});

const viewBackButton = document.getElementById("view_back_button");
viewBackButton.addEventListener("click", viewBackButtonAction);  // Goes "back" to the File List

function viewBackButtonAction(event) {
	event.preventDefault();
	fileTextContent = "";
	contentViewElement.value = "";
	showActivity("list");
}


const contentEditElement = document.getElementById("file_edit_content");


const viewEditButton = document.getElementById("view_edit_button");
viewEditButton.addEventListener("click", viewEditButtonAction);

function viewEditButtonAction(event) {
	event.preventDefault();
	contentViewElement.value = "";
	contentEditElement.value = fileTextContent;
	showActivity("edit");
}


const viewOpenButton = document.getElementById("view_open_button");
viewOpenButton.addEventListener("click", textOperationOpenButtonAction);


/* File edit */

const editBackButton = document.getElementById("edit_back_button");
editBackButton.addEventListener("click", editBackButtonAction);  // Goes "back" to the File View

function editBackButtonAction(event) {
	event.preventDefault();
	contentEditElement.value = "";
	contentViewElement.value = fileTextContent;
	showActivity("view");
}


const updateButton = document.getElementById("update_button");
updateButton.addEventListener("click", updateButtonAction);

function updateButtonAction(event) {
	event.preventDefault();
	setEditElementsDisabled(true);
	fetch(fileTextPath, {method: "PUT", body: contentEditElement.value})
		.then(() => {
			fileTextContent = contentEditElement.value;
		})
		.catch((err) => {
			alert("There was an error and the file was not updated on" +
			      "the server: " + err);
		})
		.finally(() => setEditElementsDisabled(false));
}


const retrieveButton = document.getElementById("retrieve_button");
retrieveButton.addEventListener("click", retrieveButtonAction);

function retrieveButtonAction(event) {
	event.preventDefault();
	setEditElementsDisabled(true);
	fetch(fileTextPath)
		.then((response) => response.text())
		.then((text) => {
			fileTextContent = text;
			contentEditElement.value = text;
		})
		.catch((err) => {
			alert("There was an error and the file could not be retrieved: " +
			      err);
		})
		.finally(() => setEditElementsDisabled(false));
}


function setEditElementsDisabled(bool) {
	const editElements =
		[contentEditElement, editBackButton, updateButton, retrieveButton,
		 editOpenButton];
	editElements.forEach((e) => e.disabled = bool);
}


const editOpenButton = document.getElementById("edit_open_button");
editOpenButton.addEventListener("click", textOperationOpenButtonAction);

function textOperationOpenButtonAction(event) {
	event.preventDefault();
	window.open(fileTextPath, "_blank");
}


/* Testing utility function */
/*
function timeoutPromise(ms, result) {
	return new Promise((resolve, reject) => {
		setTimeout(() => resolve(result), ms);
	});
}
*/


/* Start application */

showActivity("list");
updateFileList(baseDirectory);
